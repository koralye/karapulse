# Building from sources

## Requirements
- First you need to install some system dependencies in order to build Karapulse. The easiest way is to install the packages listed in
[the recipe of the Docker image used by CI](docker/Dockerfile#L5), just execute the `apt-get install` command on your system if you
are using Debian or Ubuntu.
- You may also have to install a few extra packages:
```
sudo apt install -y nodejs build-essential
```
- Then you'll have to [install meson](https://mesonbuild.com/Getting-meson.html):
```
pip3 install --user meson

```
- [Install Rust](https://www.rust-lang.org/learn/get-started)
```
curl https://sh.rustup.rs -sSf | sh
```
- Get the source code
```
git clone https://gitlab.freedesktop.org/gdesmott/karapulse.git
cd karapulse
```

## Building
```
meson build -D cdg-plugin=true -D prefix=$PWD/install
ninja -C build install
```
- You can now launch `./install/bin/karapulse` and `./install/bin/karapulse-manage-db`

# Building using Podman or Docker

If you prefer you can also use [Podman](https://podman.io)
or [Docker](https://www.docker.com) to build Karapulse:
```
podman run --rm -v `pwd`:`pwd` -w `pwd` --security-opt label=disable -it registry.freedesktop.org/gdesmott/karapulse/amd64/debian:latest meson build -D prefix=$PWD/install
podman run --rm -v `pwd`:`pwd` -w `pwd` --security-opt label=disable -it registry.freedesktop.org/gdesmott/karapulse/amd64/debian:latest ninja -C build install
podman run --rm -v `pwd`:`pwd` -w `pwd` --security-opt label=disable --network=host --env="DISPLAY" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" -it registry.freedesktop.org/gdesmott/karapulse/amd64/debian:latest ./install/bin/karapulse
```

The same commands should work using `docker` instead of `podman`.
You may have to run `xhost +` on the host in order to start Karapulse from the container.
