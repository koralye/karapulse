// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use anyhow::{anyhow, bail, Result};
use gstreamer as gst;
use gstreamer_pbutils as pbutils;
use pbutils::traits::DiscovererStreamInfoExt;
use std::path::Path;
use url::Url;
use walkdir::{DirEntry, WalkDir};

use crate::db::DB;

pub struct Crawler<'a> {
    db: &'a DB,
}

impl<'a> Crawler<'a> {
    pub fn new(db: &'a DB) -> Crawler {
        Crawler { db }
    }

    fn extract_artist_title_from_tags(
        &self,
        info: &pbutils::DiscovererInfo,
    ) -> Result<(String, String)> {
        let tags = match info.tags() {
            Some(tags) => tags,
            None => bail!("Media doesn't contain tags"),
        };

        let artist_val = match tags.get::<gst::tags::Artist>() {
            Some(artist) => artist,
            None => bail!("No 'artist' tag"),
        };

        let title_val = match tags.get::<gst::tags::Title>() {
            Some(title) => title,
            None => bail!("No 'title' tag"),
        };

        Ok((artist_val.get().to_string(), title_val.get().to_string()))
    }

    fn extract_artist_title_from_file_name(&self, path: &Path) -> Result<(String, String)> {
        let stem = path.file_stem().unwrap().to_str().unwrap();
        let mut split = stem.splitn(2, " - ");

        let artist = split
            .next()
            .ok_or_else(|| anyhow!("Missing artist in file name"))?;
        let title = split
            .next()
            .ok_or_else(|| anyhow!("Missing title in file name"))?;

        Ok((artist.trim().to_string(), title.trim().to_string()))
    }

    fn discover(&self, path: &Path) -> Result<pbutils::DiscovererInfo> {
        let url = Url::from_file_path(path.to_str().unwrap()).unwrap();

        let timeout: gst::ClockTime = gst::ClockTime::from_seconds(15);
        let discoverer = pbutils::Discoverer::new(timeout)?;
        let info = discoverer.discover_uri(&url.to_string())?;

        Ok(info)
    }

    pub fn add_file(&self, path: &Path) -> Result<()> {
        let path = path.canonicalize()?;
        debug!("Adding {:?}", path);
        let info = self.discover(&path)?;

        let duration = info.duration();
        let length = duration.map(|d| d.seconds() as i32);

        let (artist, title) = {
            if let Ok((artist, title)) = self.extract_artist_title_from_tags(&info) {
                (artist, title)
            } else if let Ok((artist, title)) = self.extract_artist_title_from_file_name(&path) {
                (artist, title)
            } else {
                bail!("Failed to extract artist and title from tags and file name");
            }
        };

        self.db.add_song(&path, &artist, &title, length)
    }

    fn add_mp3(&self, mp3: &Path) -> Result<()> {
        self.check_cdg(mp3).map_err(|e| anyhow!("cdg: {}", e))?;

        self.add_file(mp3)?;
        Ok(())
    }

    fn check_cdg(&self, mp3: &Path) -> Result<(), anyhow::Error> {
        let cdg = mp3.with_extension("cdg");
        let cdg = cdg.canonicalize()?;
        let info = self.discover(&cdg)?;
        let video_streams = info.video_streams();
        let video_stream = video_streams
            .get(0)
            .ok_or_else(|| anyhow!("missing video stream"))?;
        let video_caps = video_stream
            .caps()
            .ok_or_else(|| anyhow!("missing video caps"))?;
        let video_s = video_caps
            .structure(0)
            .ok_or_else(|| anyhow!("video caps is missing structure"))?;
        if video_s.name() != "video/x-cdg" {
            bail!("{:?} is not detected as CDG ({})", cdg, video_s.name());
        }

        Ok(())
    }

    fn add_video(&self, video: &Path) {
        if let Err(e) = self.add_file(video) {
            warn!("Failed to add {}: {}", video.display(), e);
        }
    }

    fn try_adding(&self, entry: Result<DirEntry, walkdir::Error>) {
        if entry.is_err() {
            return;
        }
        let entry = entry.unwrap();

        if !entry.file_type().is_file() {
            return;
        }

        let path = entry.path();
        if self.db.has_song(path).unwrap() {
            debug!("{} already in DB; skip", path.display());
            return;
        }

        match path.extension() {
            None => {}
            Some(os_str) => match os_str.to_str() {
                Some("mp3") => {
                    if let Err(e) = self.add_mp3(path) {
                        warn!("Failed to add {:?}: {}", path, e);
                    }
                }
                Some("cdg") => {} // CDG are handled with their matching mp3
                Some("mkv") | Some("mp4") | Some("webm") => self.add_video(path),
                _ => debug!("Ignore {}", path.display()),
            },
        }
    }

    pub fn add_dir(&self, path: &Path) -> Result<()> {
        debug!("Adding directory {}", path.display());

        WalkDir::new(path)
            .into_iter()
            .for_each(|entry| self.try_adding(entry));

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests::{get_media_test_dir, TestMedia};

    #[test]
    fn add_file() {
        gst::init().unwrap();

        let db = DB::new_memory().unwrap();
        let crawler = Crawler::new(&db);

        let path = TestMedia::VideoTagged.path();
        assert!(path.exists(), "path {:?} does not exist", path);
        crawler.add_file(&path).unwrap();

        let songs = db.list_songs().unwrap();
        assert_eq!(songs.len(), 1);
        assert!(songs[0].path.ends_with(path.to_str().unwrap()));
        assert_eq!(songs[0].artist, "Artist1");
        assert_eq!(songs[0].title, "Title1");
    }

    #[test]
    fn add_dir() {
        gst::init().unwrap();

        let db = DB::new_memory().unwrap();
        let crawler = Crawler::new(&db);
        let dir = get_media_test_dir();

        assert!(dir.exists(), "path {:?} does not exist", dir);
        crawler.add_dir(&dir).unwrap();

        let songs = db.list_songs().unwrap();
        assert_eq!(songs.len(), 1);
        assert_eq!(songs[0].artist, "Artist1");
        assert_eq!(songs[0].title, "Title1");
    }
}
