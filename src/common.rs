// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use std::sync::Once;

use anyhow::Result;
use gstreamer as gst;

static CDG_REGISTER: Once = Once::new();

pub fn init() -> Result<()> {
    unsafe {
        x11::xlib::XInitThreads();
    }
    gst::init()?;
    gtk::init()?;

    #[cfg(feature = "cdg-plugin")]
    {
        CDG_REGISTER
            .call_once(|| gstcdg::plugin_register_static().expect("failed to register cdg plugin"));
    }

    // Disable vaapi as it breaks rendering with gtkglsink
    let registry = gst::Registry::get();
    let plugin = registry.lookup("libgstvaapi.so");
    if let Some(plugin) = plugin {
        registry.remove_plugin(&plugin);
    }

    Ok(())
}
