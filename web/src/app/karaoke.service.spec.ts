import { TestBed } from '@angular/core/testing';

import { KaraokeService } from './karaoke.service';

describe('KaraokeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: KaraokeService = TestBed.get(KaraokeService);
    expect(service).toBeTruthy();
  });
});
