import {Injectable} from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api';
import {Observable, of} from 'rxjs';
import {log} from 'util';
import {Song} from './song';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const data = [{}];
    const play = [{}];
    const pause = [{}];
    const next = [{}];
    const search: Song[] = [{id: 0, title: 'pwet pwet', artist: 'ANnie Cordie', length: 225}, {
      id: 1,
      title: 'wowowow',
      artist: 'nirvano',
      length: 245
    }, {id: 2, title: 'Ping pong', artist: 'Technotronics', length: 12}];
    const enqueue_db = [{}];
    return {data, pause, next, search, enqueue_db};
  }

  get(request: RequestInfo): Observable<Response> {

    log('tutu');
    return of(new Response());
  }

  constructor() {
  }
}
