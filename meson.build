project('karapulse',
        'rust',
        version: '0.4.0',
        license: 'GPL3',
	meson_version : '>= 0.50')


dependency('glib-2.0', version: '>= 2.56')
dependency('gio-2.0', version: '>= 2.56')
dependency('gtk+-3.0', version: '>= 3.22')


version = meson.project_version()
version_array = version.split('.')
major_version = version_array[0].to_int()
minor_version = version_array[1].to_int()
version_micro = version_array[2].to_int()

prefix = get_option('prefix')
bindir = prefix / get_option('bindir')
localedir = prefix / get_option('localedir')

datadir = prefix / get_option('datadir')
pkgdatadir = datadir / meson.project_name()
iconsdir = datadir / 'icons'
podir =meson.source_root () / 'po'
gettext_package = meson.project_name()


if get_option('profile') == 'development'
  profile = 'Devel'
  name_suffix = ' (Devel)'
  vcs_tag = run_command('git', 'rev-parse', '--short', 'HEAD').stdout().strip()
  if vcs_tag == ''
    version_suffix = '-devel'
  else
    version_suffix = '-@0@'.format (vcs_tag)
  endif
else
  profile = ''
  name_suffix = ''
  version_suffix = ''
endif

application_id = 'org.karapulse.Karapulse@0@'.format(profile)


i18n = import('i18n')
gnome = import('gnome')

subdir('po')
subdir('data')

cargo = find_program('cargo', required: false)
cargo_script = find_program('build-aux/cargo.sh')
cargo_test_script = find_program('build-aux/cargo-test.sh')

subdir('src')

find_program('npm')
build_web = find_program('build-aux/build-web.sh')
subdir('web')

meson.add_dist_script(
  'build-aux/dist-vendor.sh',
  meson.build_root() / 'meson-dist' / meson.project_name() + '-' + version,
  meson.source_root()
)

if get_option('profile') == 'development'
    # Setup pre-commit hook for ensuring coding style is always consistent
    message('Setting up git pre-commit hook..')
    run_command('cp', '-f', 'hooks/pre-commit.hook', '.git/hooks/pre-commit')
endif


meson.add_install_script('build-aux/meson_post_install.py')
